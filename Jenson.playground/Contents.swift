import UIKit
import SpriteKit
import PlaygroundSupport

class Scene {
    var background: UIImage
    
    init(bg: UIImage){
        background = bg
    }
}

class ImageRender {
    var image: UIImage
    
    func show() {
        
    }
    
    func hide() {
        
    }
    
    init(image: UIImage){
        self.image = image
    }
}

class Character {
    var name: String
    var color: UIColor
    
    func say(_ message: String) {
        var message = message
        print(self.name, ": ", message)
    }
    
    init(name: String, color: UIColor)
    {
        self.name = name
        self.color = color
    }
}

// Initialize characters
let m = Character(name: "Monika", color: #colorLiteral(red: 0.909803926944733, green: 0.47843137383461, blue: 0.643137276172638, alpha: 1.0))
let s = Character(name: "Sayori", color: #colorLiteral(red: 0.925490200519562, green: 0.235294118523598, blue: 0.10196078568697, alpha: 1.0))

//Create scene and start game
let schoolOne = Scene(bg: #imageLiteral(resourceName: "F72117A5-46FF-4D4A-9359-2483AB05FFBA-1815-000002A0B4B4B0B5.jpeg"))
m.say("Ahaha, I'm in a game now!")
s.say("Wow! Much impress!")
