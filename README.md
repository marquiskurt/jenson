# Jenson
_Swift Visual Novel Engine_

## About this project
**Jenson** is an attempt at creating a fun, easy-to-use visual novel engine for Swift Playgrounds. Jenson's structures and classes aim to be similar to those found in _Ren'Py_, making it easy for seasoned developers to hop in right away. Jenson aims to use the `PlaygroundSupport`, `UIKit` and `SpriteKit` modules found in Swift Playgrounds to create a native iOS experience for visual novel writers.

## Features
* Takes advantage of Swift Playground's UIKit and SpriteKit support
* Uses native iOS color and image pickers
* Distributable via playground books

## Example code
```swift

let m = Character(name: "Monika", color: blue)
let a = Character(name: "Alice Angel", color: yellow)

let school = Scene(bg: 'image.png')

m.say("Hi, Alice!")
a.say("Monika, stay away from me.")
a.say("I don't want you to touch me with those inky hands...")

```